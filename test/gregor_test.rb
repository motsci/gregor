require 'test_helper'

class GregorTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Gregor::VERSION
  end

  def test_that_it_has_a_default_logger
    assert_kind_of ::Logger, ::Gregor.logger
    assert_kind_of ::Gregor::Logger::NullLogger, ::Gregor.logger
  end

  def test_that_a_logger_cannot_be_nil
    ::Gregor.logger = nil
    refute_nil ::Gregor.logger
    assert_kind_of ::Gregor::Logger::NullLogger, ::Gregor.logger
  end
end
