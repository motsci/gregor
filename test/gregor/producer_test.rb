require 'test_helper'

module Gregor
  class ProducerTest < Minitest::Test
    def test_it_raises_ProducerShutdown_if_we_try_to_send_to_a_shutdown_producer
      p = Producer.new({ brokers: "host:port", client_id: "client_id" })
      p.close
      assert_raises(Errors::ProducerShutdownError) { p.send([]) }
    end
  end
end
