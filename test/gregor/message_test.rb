require 'test_helper'

module Gregor
  class MessageTest < Minitest::Test
    def test_that_it_provides_access_to_topic_value_key
      mts = Message.new({ topic: "hello_topic", value: "Hello World", key: "key" })
      assert_equal "hello_topic", mts.topic
      assert_equal "Hello World", mts.value
      assert_equal "key", mts.key
    end
  end
end
