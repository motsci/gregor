require "forwardable"
require "poseidon"

require "gregor/version"

module Gregor
  module Errors
    # Raised when a custom partitioner tries to send
    # a message to a partition that doesn't exist.
    class InvalidPartitionError < StandardError; end

    # Raised when we are unable to fetch metadata from
    # any of the brokers.
    class UnableToFetchMetadata < StandardError; end

    # Raised when a messages checksum doesn't match
    class ChecksumError < StandardError; end

    # Raised when you try to send messages to a producer
    # object that has been #shutdown
    class ProducerShutdownError < StandardError; end
  end

  # Default logger, does nothing but extend std Ruby logger
  module Logger
    class NullLogger < ::Logger
      def initialize(*args)
      end

      def add(*args, &block)
      end
    end
  end

  def self.logger
    @logger ||= Logger::NullLogger.new
  end

  def self.logger=(logger)
    @logger = logger
  end
end

require_relative "gregor/message"
require_relative "gregor/producer"
require_relative "gregor/consumer"
