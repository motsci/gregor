module Gregor
  class Message
    attr_reader :topic, :value, :key

    def initialize(args)
      raise ArgumentError, "Must provide a non-nil topic" if args[:topic].nil?
      @topic  = args[:topic]
      @value  = args[:value]
      @key    = args[:key]
    end
  end
end
