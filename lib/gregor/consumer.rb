module Gregor
  class Consumer
    extend ::Forwardable

    def_delegators :client, :fetch, :next_offset, :close

    def initialize(args, options = {})
      @client = Poseidon::PartitionConsumer.new(args[:client_id], args[:host], args[:port],
                                                args[:topic], args[:partition], args[:offset],
                                                options)
    end

    private
    attr_reader :client
  end
end

