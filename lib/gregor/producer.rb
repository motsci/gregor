module Gregor
  class Producer
    extend ::Forwardable

    def_delegator :client, :close

    def initialize(args, options = {})
      @client = Poseidon::Producer.new(Array(args[:brokers]), args[:client_id], options)
    end

    def send(messages)
      msgs = Array(messages)
      client.send_messages(msgs)
      Gregor.logger.debug("Sent messages: #{msgs.map(&:inspect)}")
    rescue Poseidon::Errors::ProducerShutdownError => e
      raise Errors::ProducerShutdownError, e.message
    end

    private
    attr_reader :client
  end
end
